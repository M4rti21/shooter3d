using System;
using Enemy;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Player
{
    [RequireComponent(typeof(PlayerMovement))]
    [RequireComponent(typeof(PlayerShoot))]
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour, IDamageable
    {
        [SerializeField] private InputActionAsset inputAsset;
        [SerializeField] private new GameObject camera;

        [SerializeField] private Camera cam1;
        [SerializeField] private Camera cam2;

        private bool cam = true;
        
        [SerializeField] private int hp;
        public GameObject Camera => camera;
        public InputActionAsset Input { get; private set; }

        //COMPONENTS
        private PlayerMovement _mv;
        private PlayerShoot _sh;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            InitComponents();
            InitInputs();
        }

        private void InitComponents()
        {
            _mv = GetComponent<PlayerMovement>();
            _sh = GetComponent<PlayerShoot>();
        }

        private void InitInputs()
        {
            Input = Instantiate(inputAsset);
            InputActionMap gameplay = Input.FindActionMap("gameplay");
            gameplay.FindAction("shootl").started += _sh.ShootL;
            gameplay.FindAction("shootr").started += _sh.ShootR;
            gameplay.FindAction("changecam").started += ToggleCamera;
            gameplay.Enable();
        }

        private void ToggleCamera(InputAction.CallbackContext c)
        {
            cam = !cam;
            if (cam)
            {
                cam2.enabled = false;
                cam2.gameObject.SetActive(false);
                
                cam1.gameObject.SetActive(true);
                cam1.enabled = true;
            }
            else
            {
                cam1.enabled = false;
                cam1.gameObject.SetActive(false);

                cam2.gameObject.SetActive(true);
                cam2.enabled = true;
            }
        }
        
        private void Update()
        {
            _mv.Move();
        }

        public event Action<int> OnDamage;

        public void Damage(int dmg)
        {
            hp -= dmg;
            SceneManager.LoadScene("GameOver");
            if (hp <= 0) Destroy(gameObject);
        }
    }
}