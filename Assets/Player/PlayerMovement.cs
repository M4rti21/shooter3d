using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        private PlayerController _pc;
        private Rigidbody _rb;
        private InputAction _mv;
        private InputAction _delta;
        private float _camAngle;

        [SerializeField] private int rotationSpd;
        [SerializeField] private int spd;

        private void Start()
        {
            _pc = GetComponent<PlayerController>();
            InputActionMap gameplay = _pc.Input.FindActionMap("gameplay");
            _mv = gameplay.FindAction("move");
            _delta = gameplay.FindAction("camera");
            _rb = GetComponent<Rigidbody>();
        }

        public void Move()
        {
            Vector2 mouseVector = rotationSpd * Time.deltaTime * _delta.ReadValue<Vector2>();
            transform.Rotate(Vector3.up * mouseVector.x);

            _camAngle += mouseVector.y;
            _camAngle = Mathf.Clamp(_camAngle, -75, 75);
            _pc.Camera.transform.localEulerAngles = new Vector3(-_camAngle, 0, 0);

            Transform t = transform;
            Vector3 f = t.forward * _mv.ReadValue<Vector2>().y;
            Vector3 s = t.right * _mv.ReadValue<Vector2>().x;
            _rb.velocity = (f + s).normalized * spd + Vector3.up * _rb.velocity.y;
        }
    }
}