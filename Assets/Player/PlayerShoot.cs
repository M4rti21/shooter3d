using Enemy;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerShoot : MonoBehaviour
    {
        private PlayerController _pc;
        [SerializeField] private int damage;

        [Header("Shoot L")] [SerializeField] private int recoil;

        [Header("Shoot R")] [SerializeField] private int bullets;

        [SerializeField] private float area;

        private void Start()
        {
            _pc = GetComponent<PlayerController>();
        }

        public void ShootL(InputAction.CallbackContext c)
        {
            Vector3 origin = _pc.Camera.transform.position;
            Vector3 direction = _pc.Camera.transform.forward;
            _pc.Camera.transform.localEulerAngles = new Vector3(-_pc.Camera.transform.localEulerAngles.x -recoil, 0, 0);
            _pc.transform.Rotate(Vector3.up * Random.Range(-recoil, recoil));

            if (!Physics.Raycast(origin, direction, out var hit, 20f)) return;
            
            Debug.DrawLine(origin, hit.point, Color.green, 2f);

            if (!hit.collider.TryGetComponent(out IDamageable targ)) return;
            targ.Damage(damage);
            if (!hit.collider.TryGetComponent(out IPushable push)) return;
            push.Push(_pc.Camera.transform.forward, 10);
            
            Debug.Log($"{hit.collider.gameObject} impactat per un valor de {damage} punts de mal.");

        }

        public void ShootR(InputAction.CallbackContext c)
        {
            Vector3 origin = _pc.Camera.transform.position;
            for (int i = 0; i < bullets; i++)
            {
                Transform t = _pc.Camera.transform;
                Vector3 direction = t.forward;
                Vector3 xDispersion = Random.Range(-area, area) * t.right;
                Vector3 yDispersion = Random.Range(-area, area) * t.up;
                direction += xDispersion + yDispersion;
                direction.Normalize();
                
                if (!Physics.Raycast(origin, direction, out var hit, 20f)) return;
                
                Debug.DrawLine(origin, hit.point, Color.red, 2f);
                
                if (hit.collider.TryGetComponent(out IDamageable target)) target.Damage(damage);
            }
        }
    }
}