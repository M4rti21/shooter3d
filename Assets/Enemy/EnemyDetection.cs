using UnityEngine;

namespace Enemy
{
    public class EnemyDetection : MonoBehaviour
    {
        [SerializeField] private EnemyController ec;
        
        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            if (Vector3.Distance(transform.position, other.transform.position) < 2) return;
            ec.Target = other.gameObject;
            ec.ChangeState(EnemyStates.Persecute);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            ec.ChangeState(EnemyStates.Patrol);
        }
    }
}
