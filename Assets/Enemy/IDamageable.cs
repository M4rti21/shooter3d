using System;

namespace Enemy
{
    public interface IDamageable
    {
        public event Action<int> OnDamage;
        public void Damage(int dmg);
    }
}
