using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;


namespace Enemy
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyController : MonoBehaviour
    {

        [SerializeField] private EnemyStates currentState;
        [SerializeField] private GameObject target;
        [SerializeField] private int damage;
        [SerializeField] private GameObject waypoints;
        [SerializeField] private Vector3[] wps;
        
        private Animator _animator;

        private Collider[] _ragdollColliders;
        private Rigidbody[] _limbsRigidbodies;
        public GameObject Target
        {
            get => target;
            set => target = value;
        }

        private int _index;
        private NavMeshAgent _nma;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _animator.Play("EnemyIdle");
            _nma = GetComponent<NavMeshAgent>();
            GetWaypoints();
        }

        private void Start()
        {
            ChangeState(EnemyStates.Patrol);
        }

        private void Update()
        {
            UpdateState();
        }

        private void GetWaypoints()
        {
            wps = new Vector3[waypoints.transform.childCount];
            for (int i = 0; i < wps.Length; i++)
            {
                wps[i] = waypoints.transform.GetChild(i).transform.position;
            }
        }
        
        public void Shoot()
        {
            var t = transform;
            Vector3 origin = t.position;
            Vector3 direction = t.forward;

            if (!Physics.Raycast(origin, direction, out var hit, 20f)) return;
            
            Debug.DrawLine(origin, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent(out IDamageable target)) target.Damage(damage);
        }
        
        private void PatrolMove()
        {
            _index = Random.Range(0, wps.Length);
            MoveTo(wps[_index]);
        }

        private void MoveTo(Vector3 point)
        {
            _nma.SetDestination(point);
        }
        
        public void ChangeState(EnemyStates newState)
        {
            if (currentState == EnemyStates.Dead) return;
            ExitState();
            InitState(newState);
        }

        private void InitState(EnemyStates state)
        {
            currentState = state;
            switch (currentState)
            {
                case EnemyStates.Idle:
                    _animator.Play("EnemyIdle");
                    _nma.isStopped = true;
                    break;
                case EnemyStates.Patrol:
                    _animator.Play("EnemyWalk");
                    _nma.isStopped = false;
                    PatrolMove();
                    break;
                case EnemyStates.Persecute:
                    _animator.Play("EnemyWalk");
                    _nma.isStopped = false;
                    MoveTo(target.transform.position);
                    break;
                case EnemyStates.Attack:
                    _nma.isStopped = true;
                    _animator.Play("EnemyShoot");
                    break;
                case EnemyStates.Dead:
                    _animator.StopPlayback();
                    _nma.isStopped = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void UpdateState()
        {
            switch (currentState)
            {
                case EnemyStates.Idle:
                    break;
                case EnemyStates.Patrol:
                    if (Vector3.Distance(transform.position, wps[_index]) < 1) PatrolMove();
                    break;
                case EnemyStates.Persecute:
                    MoveTo(target.transform.position);
                    break;
                case EnemyStates.Attack:
                    break;
                case EnemyStates.Dead:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ExitState()
        {
            switch (currentState)
            {
                case EnemyStates.Idle:
                    break;
                case EnemyStates.Patrol:
                    break;
                case EnemyStates.Persecute:
                    break;
                case EnemyStates.Attack:
                    break;
                case EnemyStates.Dead:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}