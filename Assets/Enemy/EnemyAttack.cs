using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyAttack : MonoBehaviour
    {   
        [SerializeField] private EnemyController ec;
        
        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            if (Vector3.Distance(transform.position, other.transform.position) < 2) return;
            ec.Target = other.gameObject;
            ec.ChangeState(EnemyStates.Attack);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            ec.ChangeState(EnemyStates.Persecute);
        }
    }
}
