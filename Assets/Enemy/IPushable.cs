using UnityEngine;

public interface IPushable
{
    void Push(Vector3 direction, float impulse);
}
