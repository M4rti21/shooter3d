using System;
using UnityEngine;

namespace Enemy
{
    public class RagdollController : MonoBehaviour
    {
        private int m_HP = 100;

        [SerializeField]
        private GameObject m_RigRoot;
        private Rigidbody[] m_Bones;
        private Animator m_Animator;

        private void Awake()
        {
            m_Animator = GetComponent<Animator>();
            m_Bones = m_RigRoot.GetComponentsInChildren<Rigidbody>();
            Activate(false);

            foreach (Rigidbody bone in m_Bones)
                if(bone.TryGetComponent(out IDamageable damageable))
                    damageable.OnDamage += Damage;
        }

        void Activate(bool state)
        {
            foreach (Rigidbody bone in m_Bones)
                bone.isKinematic = !state;
            m_Animator.enabled = !state;
        }

        private void Die()
        {
            foreach (Rigidbody bone in m_Bones)
                if (bone.TryGetComponent(out IDamageable damageable))
                    damageable.OnDamage -= Damage;

            Activate(true);
            GetComponent<EnemyController>().ChangeState(EnemyStates.Dead);
        }
        public void Damage(int dmg)
        {
            m_HP -= dmg;
            if(m_HP <= 0) Die();
        }
    }
}
