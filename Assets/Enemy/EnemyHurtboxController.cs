using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyHurtboxController : MonoBehaviour, IDamageable, IPushable
    {

        public event Action<int> OnDamage;
        public void Damage(int dmg)
        {
            OnDamage.Invoke(dmg);
        }

        public void Push(Vector3 direction, float impulse)
        {
            GetComponent<Rigidbody>().AddForce(direction * impulse, ForceMode.Impulse);
        }
    }
}
